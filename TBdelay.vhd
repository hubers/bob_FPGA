--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:42:50 06/15/2015
-- Design Name:   
-- Module Name:   C:/Users/e18user/Desktop/bradniels/FPGA/TBdelay.vhd
-- Project Name:  bradniels
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bradniels_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

ENTITY TBdelay IS
  END TBdelay;

ARCHITECTURE behavior OF TBdelay IS 

    -- Component Declaration for the Unit Under Test (UUT)

  COMPONENT bradniels_top
    PORT(
      CLK : IN  std_logic;
      LED : OUT  std_logic_vector(7 downto 0);
      LEMO_IN : IN  std_logic_vector(3 downto 0);
      LEMO_OUT : OUT  std_logic_vector(3 downto 0);
      SWA_DRVPOS : OUT  std_logic_vector(1 downto 0);
      SWA_DRVNEG : OUT  std_logic_vector(1 downto 0);
      SWB_DRVPOS : OUT  std_logic_vector(1 downto 0);
      SWB_DRVNEG : OUT  std_logic_vector(1 downto 0);
      CONTROL_DATAA : IN  std_logic_vector(35 downto 0);
      CONTROL_DATAB : IN  std_logic_vector(35 downto 0);
      CONTROL_DELAY : IN  std_logic_vector(17 downto 0);
      IO : OUT  std_logic_vector(7 downto 0)
    );
  END COMPONENT;


   --Inputs
  signal CLK : std_logic := '0';
  signal LEMO_IN : std_logic_vector(3 downto 0) := (others => '0');
  signal CONTROL_DATAA : std_logic_vector(35 downto 0) := x"1000_0080"&"0000";
  signal CONTROL_DATAB : std_logic_vector(35 downto 0) := (others => '0');
  signal CONTROL_DELAY : std_logic_vector(17 downto 0) := x"000f" & "01";

   --Outputs
  signal LED : std_logic_vector(7 downto 0);
  signal LEMO_OUT : std_logic_vector(3 downto 0);
  signal SWA_DRVPOS : std_logic_vector(1 downto 0);
  signal SWA_DRVNEG : std_logic_vector(1 downto 0);
  signal SWB_DRVPOS : std_logic_vector(1 downto 0);
  signal SWB_DRVNEG : std_logic_vector(1 downto 0);
  signal IO : std_logic_vector(7 downto 0);

   -- Clock period definitions
  constant CLK_period : time := 10 ns;

BEGIN

   -- Instantiate the Unit Under Test (UUT)
  uut: bradniels_top PORT MAP (
    CLK => CLK,
    LED => LED,
    LEMO_IN => LEMO_IN,
    LEMO_OUT => LEMO_OUT,
    SWA_DRVPOS => SWA_DRVPOS,
    SWA_DRVNEG => SWA_DRVNEG,
    SWB_DRVPOS => SWB_DRVPOS,
    SWB_DRVNEG => SWB_DRVNEG,
    CONTROL_DATAA => CONTROL_DATAA,
    CONTROL_DATAB => CONTROL_DATAB,
    CONTROL_DELAY => CONTROL_DELAY,
    IO => IO
  );

   -- Clock process definitions
  CLK_process :process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  END;

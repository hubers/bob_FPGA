----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    18:41:51 04/02/2013
-- Design Name:
-- Module Name:    single_switch - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity single_switch is
  port (
    CLK    : in std_logic;
    control_data: in std_logic_vector(35 downto 0);
    DRVPOS : out std_logic
  );
end single_switch;

architecture Behavioral of single_switch is

  constant maximum    : integer := 2**18 - 1;
  signal max_int      : integer                         := to_integer(unsigned(control_data(35 downto 18)));
  signal duty         : integer range 0 to  maximum     := to_integer(unsigned(control_data(17 downto 0)));
  constant max_del    : integer                         := 127;
  constant flop_delay : integer range 0 to max_del      := 5;

  signal counter      : integer range maximum  downto 0 := maximum;
  signal iPos         : std_logic                       := '0';
  signal iNeg         : std_logic                       := '0';

begin

  max_int <= to_integer(unsigned(control_data(35 downto 18)));
  duty    <= to_integer(unsigned(control_data(17 downto 0)));


  process(CLK)
  begin
    if rising_edge(CLK) then
      if counter = 0 then
        iPos <='1';
        iNeg <='0';
      elsif counter = duty then
        iPos <= '0';
        iNeg <= '1';
      end if;
    end if;
  end process;

  process(CLK)
  begin
    if rising_edge(CLK) then
      if counter = 0 then
        counter <= max_int;
      else
        counter <= counter-1;
      end if;
    end if;
  end process;

  DRVPOS <= iPos;

end Behavioral;


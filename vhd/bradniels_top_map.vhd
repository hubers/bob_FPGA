-----------------------------------------------------------------------------
-- Title      : Top map
-- Project    : Bradniels
-----------------------------------------------------------------------------
-- File       : bradniels_top_map.vhd
-- Author     : Stefan Huber
-- Company    : TU München, Physik Department E18
-- Created    : 2013/03/22
-- Last update: 2017-08-08  16:36
-- Platform   : Xilinx ISE
-- Chip       : xc3s50an
-----------------------------------------------------------------------------
-- Copyright (c) 2013
-----------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013/03/22  1.0      shuber	Created
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.vcomponents.all;


entity bradniels_top_map is
  port (
    CLK_IN     : in  std_logic;
    LED        : out std_logic_vector(7 downto 0);
    LEMO_IN    : in  std_logic_vector(3 downto 0);
    LEMO_OUT   : out std_logic_vector(3 downto 0);
    SWA_DRVPOS : out std_logic_vector(1 downto 0);
    SWA_DRVNEG : out std_logic_vector(1 downto 0);
    SWB_DRVPOS : out std_logic_vector(1 downto 0);
    SWB_DRVNEG : out std_logic_vector(1 downto 0);
    IO         : out std_logic_vector(7 downto 0)
  );
end bradniels_top_map;



architecture arch of bradniels_top_map is

  signal CLK           : std_logic;
  signal lCLK          : std_logic;
  signal CONTROL_DATA  : std_logic_vector(35 downto 0);
  signal CONTROL_DELAY : std_logic_vector(17 downto 0);
  signal CONTROL_EN    : std_logic_vector(1 downto 0) := (others=>'1');
  signal CONTROL_INV   : std_logic_vector(1 downto 0) := (others=>'1');
begin

  BUFG_inst : BUFG
  port map(
    I => CLK_IN,
    O => lCLK
  );

  Inst_clk_gen: ENTITY work.clk_gen
  PORT MAP(
    CLKIN_IN  => lCLK,
    CLKFX_OUT => CLK
  );

  IO<=(others=>'0');
  control_inst : ENTITY work.control
  port map (
    CLK           => CLK,
    CONTROL_OUTA  => CONTROL_DATA,
    CONTROL_DELAY => CONTROL_DELAY,
    CONTROL_EN    => CONTROL_EN,
    CONTROL_INV   => CONTROL_INV
  );

  bradniels_top_inst : ENTITY work.bradniels_top
  port map (
    CLK           => CLK,
    LED           => LED,
    LEMO_IN       => LEMO_IN,
    LEMO_OUT      => LEMO_OUT,
    SWA_DRVPOS    => SWA_DRVPOS,
    SWA_DRVNEG    => SWA_DRVNEG,
    SWB_DRVPOS    => SWB_DRVPOS,
    SWB_DRVNEG    => SWB_DRVNEG,
    CONTROL_DATA  => CONTROL_DATA,
    CONTROL_DELAY => CONTROL_DELAY,
    CONTROL_EN    => CONTROL_EN,
    CONTROL_INV   => CONTROL_INV,
    IO            => open--IO
  );

end arch;

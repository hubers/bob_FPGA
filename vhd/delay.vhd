----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    13:53:59 08/03/2015
-- Design Name:
-- Module Name:    delay - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity delay is
  Port (
    CLK    : in  STD_LOGIC;
    START  : in  STD_LOGIC;
    DELAY  : in  STD_LOGIC_VECTOR (17 downto 0);
    DOUT   : out STD_LOGIC
  );
end delay;

architecture Behavioral of delay is

signal start_del       : std_logic;
constant maximum       : integer := 2**18 - 1;
signal cnt_delay_start : integer range 0 to maximum;
signal cnt_delay_stop  : integer range 0 to maximum;

begin

  process(CLK)
  begin
    if rising_edge(CLK) then
      start_del <= START;

      if start_del = '0' and START = '1' then
        cnt_delay_start <= 0;
      elsif cnt_delay_start = to_integer(unsigned(DELAY)) then
        cnt_delay_start <= maximum;
        DOUT <= '1';
      elsif cnt_delay_start /= to_unsigned(maximum,18) then
        cnt_delay_start <= cnt_delay_start + 1;
      end if;

      if start_del = '1' and START = '0' then
        cnt_delay_stop <= 0;
      elsif cnt_delay_stop = to_integer(unsigned(DELAY)) then
        cnt_delay_stop <= maximum;
        DOUT <= '0';
      elsif cnt_delay_stop /= to_unsigned(maximum,18) then
        cnt_delay_stop <= cnt_delay_stop + 1;
      end if;
    end if;
  end process;


end Behavioral;


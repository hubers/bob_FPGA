-----------------------------------------------------------------------------
-- Title      : Control
-- Project    : Bradniels
-----------------------------------------------------------------------------
-- File       : control.vhd
-- Author     : Stefan Huber
-- Company    : TU München, Physik Department E18
-- Created    : 2013/03/22
-- Last update: 2017-08-08  16:38
-- Platform   : Xilinx ISE
-- Chip       : xc3s50an
-----------------------------------------------------------------------------
-- Copyright (c) 2013
-----------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013/03/22  1.0      shuber	Created
-----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity control is
  port (
    CLK           : in STD_LOGIC;
    CONTROL_OUTA  : out std_logic_vector(35 downto 0);
    CONTROL_DELAY : out std_logic_vector(17 downto 0);
    CONTROL_EN    : out std_logic_vector(1 downto 0);
    CONTROL_INV    : out std_logic_vector(1 downto 0)
  );
end control;

architecture Behavioral of control is
  component ICON
    PORT (
      CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0)
    );
  end component;

  component VIO
    PORT (
      CONTROL  : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CLK      : IN STD_LOGIC;
      SYNC_OUT : OUT STD_LOGIC_VECTOR(89 DOWNTO 0)
    );
  end component;

  signal CONTROL0 : STD_LOGIC_VECTOR(35 downto 0);
  signal control_data : std_logic_vector(89 downto 0);

begin

  VIO_insta : VIO
  port map (
    CONTROL         => CONTROL0,
    CLK             => CLK,
    SYNC_OUT        => control_data
  );

  CONTROL_OUTA  <= control_data(35 downto 0);
  CONTROL_DELAY <= control_data(87 downto 70);
  CONTROL_EN    <= control_data(37 downto 36);
  CONTROL_INV   <= control_data(89 downto 88);

  ICON_inst : ICON
  port map (
    CONTROL0 => CONTROL0
  );


end Behavioral;


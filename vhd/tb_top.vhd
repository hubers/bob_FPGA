--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:24:21 03/21/2013
-- Design Name:   
-- Module Name:   /home/shuber/Desktop/bradniels_vhd/bradniels/vhd/tb_top.vhd
-- Project Name:  bradniels
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bradniels_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY tb_top IS
END tb_top;
 
ARCHITECTURE behavior OF tb_top IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bradniels_top
    PORT(
         CLK : IN  std_logic;
         LED : OUT  std_logic_vector(7 downto 0);
         LEMO_IN : IN  std_logic_vector(3 downto 0);
         LEMO_OUT : OUT  std_logic_vector(3 downto 0);
         SWA_DRVPOS : OUT  std_logic_vector(1 downto 0);
         SWA_DRVNEG : OUT  std_logic_vector(1 downto 0);
         SWB_DRVPOS : OUT  std_logic_vector(1 downto 0);
         SWB_DRVNEG : OUT  std_logic_vector(1 downto 0);
         IO : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal LEMO_IN : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal LED : std_logic_vector(7 downto 0);
   signal LEMO_OUT : std_logic_vector(3 downto 0);
   signal SWA_DRVPOS : std_logic_vector(1 downto 0);
   signal SWA_DRVNEG : std_logic_vector(1 downto 0);
   signal SWB_DRVPOS : std_logic_vector(1 downto 0);
   signal SWB_DRVNEG : std_logic_vector(1 downto 0);
   signal IO : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 25 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bradniels_top PORT MAP (
          CLK => CLK,
          LED => LED,
          LEMO_IN => LEMO_IN,
          LEMO_OUT => LEMO_OUT,
          SWA_DRVPOS => SWA_DRVPOS,
          SWA_DRVNEG => SWA_DRVNEG,
          SWB_DRVPOS => SWB_DRVPOS,
          SWB_DRVNEG => SWB_DRVNEG,
          IO => IO
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;

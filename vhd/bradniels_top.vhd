-----------------------------------------------------------------------------
-- Title      : Top
-- Project    : Bradniels
-----------------------------------------------------------------------------
-- File       : bradniels_top.vhd
-- Author     : Stefan Huber
-- Company    : TU München, Physik Department E18
-- Created    : 2013/03/22
-- Last update: 2017-08-08  16:38
-- Platform   : Xilinx ISE
-- Chip       : xc3s50an
-----------------------------------------------------------------------------
-- Copyright (c) 2013
-----------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013/03/22  1.0      shuber  Created
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bradniels_top is
  port (
    CLK           : in  std_logic;
    LED           : out std_logic_vector(7 downto 0);
    LEMO_IN       : in  std_logic_vector(3 downto 0);
    LEMO_OUT      : out std_logic_vector(3 downto 0);
    SWA_DRVPOS    : out std_logic_vector(1 downto 0);
    SWA_DRVNEG    : out std_logic_vector(1 downto 0);
    SWB_DRVPOS    : out std_logic_vector(1 downto 0);
    SWB_DRVNEG    : out std_logic_vector(1 downto 0);
    CONTROL_DATA  : in std_logic_vector(35 downto 0);
    CONTROL_DELAY : in std_logic_vector(17 downto 0);
    CONTROL_EN    : in std_logic_vector(1 downto 0);
    CONTROL_INV   : in std_logic_vector(1 downto 0);
    IO            : out std_logic_vector(7 downto 0)
  );
end bradniels_top;



architecture arch of bradniels_top is

  signal DRV_A, DRV_A_tmp  : std_logic;
  signal DRV_B, DRV_B_tmp  : std_logic;

begin
  LED(7 downto 4) <= b"1010";
  LED(3 downto 0) <= LEMO_IN;

  SWA0_inst : entity work.single_switch
  port map (
    CLK          => CLK,
    control_data => CONTROL_DATA,
    DRVPOS       => DRV_A_tmp
  );

  DELAY_inst : entity work.delay
  port map (
    CLK        => CLK,
    START      => DRV_A_tmp,
    DELAY      => CONTROL_DELAY,
    DOUT       => DRV_B_tmp
  );
  DRV_A        <= not DRV_A_tmp when CONTROL_INV(0) = '1' else DRV_A_tmp;
  DRV_B        <= not DRV_B_tmp when CONTROL_INV(1) = '1' else DRV_B_tmp;

  SWA_DRVPOS(0) <=     DRV_A when CONTROL_EN(0) = '1' else '0';
  SWA_DRVPOS(1) <=     DRV_B when CONTROL_EN(1) = '1' else '0';
  SWA_DRVNEG(0) <= not DRV_A when CONTROL_EN(0) = '1' else '0';
  SWA_DRVNEG(1) <= not DRV_B when CONTROL_EN(1) = '1' else '0';
  SWB_DRVPOS(0) <=     DRV_A when CONTROL_EN(0) = '1' else '0';
  SWB_DRVPOS(1) <=     DRV_B when CONTROL_EN(1) = '1' else '0';
  SWB_DRVNEG(0) <= not DRV_A when CONTROL_EN(0) = '1' else '0';
  SWB_DRVNEG(1) <= not DRV_B when CONTROL_EN(1) = '1' else '0';
  LEMO_OUT(0)   <= not DRV_A;
  LEMO_OUT(1)   <= not DRV_A;
  LEMO_OUT(2)   <= not DRV_A;
  LEMO_OUT(3)   <= not DRV_B;

end arch;
